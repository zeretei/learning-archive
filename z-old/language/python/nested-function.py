# nested function
def greet(msg):

    # Inside function
    def printer():
        print(msg)

    # return printer()
    return printer

# assign a function to variable
another = greet("Hello")

another() # Hello