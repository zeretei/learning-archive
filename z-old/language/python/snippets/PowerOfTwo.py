class PowerOfTwo:
    """Iterator of power of two"""

    # set max iteration
    def __init__(self, max = 0):
     self.max = max
  
    # return iterable
    def __iter__(self):
     self.n = 0 # set current iterable
     return self
    
    # get next iteration
    def __next__(self):
      # check if next iteration isn't greater than max
      if self.n <= self.max:
        result = 2 ** self.n
        self.n += 1
        return result
      else:
        raise StopIteration

a = PowerOfTwo(4)
i = iter(a)

print(next(i)) # 1
print(next(i)) # 2
print(next(i)) # 4
print(next(i)) # 8
print(next(i)) # 16

print(next(i)) # Error! StopIteration raised