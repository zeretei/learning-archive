# set max iteration
def PowerOfTwoGenerator(max = 0):
    # set start iterator
    n = 0

    # loop iterable till max
    while n < max:

        # yield result
        yield 2 ** n

        # increment iterator
        n += 1

# call generator
iterable =  PowerOfTwoGenerator(5)

# loop through generator
for i in iterable:
    print(i) # 1, 2, 4, 8, 16
