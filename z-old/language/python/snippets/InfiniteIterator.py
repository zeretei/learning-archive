class InfiniteIterator:
    "Infinite iterator"
    def __iter__(self):
        self.num = 1
        return self
    
    def __next__(self):
      num = self.num
      self.num += 1
      return num

a = InfiniteIterator()
i = iter(a)

print(next(i)) # 1
print(next(i)) # 2 
print(next(i)) # 3
print(next(i)) # 4

# ...
