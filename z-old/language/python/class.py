# create a class
class MyClass:

    # createt a variable
    name = "John"
    age = 55
    is_admin = True

    # create a constructor
    def __init__(self):
        pass

    # create a method
    def walk():
        print("walking...")

    def greet(name):
        print(f'Hello, {name}')

    def sum(self, a, b)
        return a + b


# create an object instance
obj = new MyClass()

# get object name property
obj.name

# set object name property
obj.name = 'Jane'

# call object greet method
obj.greet('Jane')

# MyClass.sum(obj, 5, 5)
obj.sum(5,5);

# object automatically created
MyClass.walk()

# delete an object property
del obj.age

# delete an object
del obj