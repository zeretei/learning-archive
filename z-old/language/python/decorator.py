def make_pretty(fn):
    
    def inner():
        print("I got decorated")
        fn()
   
    return inner

# @: use decorator
@make_pretty
def ordinary():
  print("I am ordinary")

# same as may_pretty(ordinary())
ordinary()

# Result:
#
# I got decorated
# I am ordinary



# @property
# @property.getter
# @property.setter
# @property.deleter
property(fget=None, fset=None, fdel=None, doc=None)
# fget: get value of attribute
# fset: set value of attribute
# fdel: delete the attribute
# doc: string(comment)

# Using @property decorator
class Celsius:
    def __init__(self, temperature=0):
        self.temperature = temperature

    def to_fahrenheit(self):
        return (self.temperature * 1.8) + 32

    @property
    def temperature(self):
        print("Getting value...")
        return self._temperature

    @temperature.setter
    def temperature(self, value):
        print("Setting value...")
        if value < -273.15:
            raise ValueError("Temperature below -273 is not possible")
        self._temperature = value
    
    # TODO: learn deleter
    @temperature.deleter
    def temperature(self):
        print("Deletting property...")
        # del self.temperature 


# create an object
human = Celsius(37) # setting value...

print(human.temperature) # getting value..., 37

print(human.to_fahrenheit()) # getting value..., 

# not working
del human.temperature