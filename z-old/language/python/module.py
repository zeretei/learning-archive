
# CREATE a module

# file1.py
def add(a, b):
    result = a + b
    return result

# IMPORT a module

# main.py
import file1

sum = file1.add(5, 5)


#########################

# rename module
import math as m 

# import specific from module
from math import pi, e

# check built-in methods
dir(math)