# open file | relative path
f = open("test.txt")

# absolute path
f = open("C:/Python33/test.txt")

# MODES
f = open("test.txt", mode="r")

# r: read (default)
# w: write
# a: append
# x: execute
# t: text mode
# b: binary mode (img & binary files)
# +: updating mode

# encoding
f = open("test.txt", mode="r" encoding="utf-8")

# close a file
f.close()

# safer way to close a file
try:
    f.open("test.txt")
finally:
    f.close()

# best way to close a file
with open("test.txt") as f # auto close

# write to file
with open('test.txt', mode='w', encoding='utf-8') as f 
f.write('line 1\n')
f.write('line 2\n')
f.write('line 3\n')

# read file
with open('test.txt', mode='r', encoding='utf-8') as f 
f.read()

f.seek(0) # change file cursor position
f.tell() # get current file position

# read file line by line
with open('test.txt', 'r') as f 
for line in f:
    print(line)

# read line alternative
f.readline() # line 1
f.readline() # line 2
f.readline() # line 3