# create a base class
class BaseClass():
    pass

# extend from base class
class DerivedClass(BaseClass):
    pass


class A:
    def __init__(self):
        pass
    
    def walk():
        pass

    def speak():
        print("from A")

class B(A):
    # calling parent methods
    def __init__(self):
        A.__init__(self);
    
    # method overriding
    def speak():
        print("from B")
    
    # override + call parent
    def greet():
        A.walk()

# check if obj is an instance of class MyClass
isinstance(obj, MyClass) 

# check if class B is a subclass of class A
issubclass(B, A)
