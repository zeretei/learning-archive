# try..except..finally
try:
    # code...
    pass

# specify exception and rename as err
except NameError as err:
    # catch an exception
    pass

except:
    # catch any exception
    pass    

finally:
    # execute after try..except
    pass

# built-in exceptions
try:
  pass
except ZeroDivisionError:
    pass
except OverflowError:
    pass
except FileNotFoundError:
    pass
finally:
    pass

# raise an exception
raise Exception("Something went wrong.")

# create a custom exception
class MyException(Exception):
    pass

#  use custom exception
raise MyException()
raise MyException("custom exception")
