const has = Object.prototype.hasOwnProperty; // cache the lookup once, in module scope.
console.log(has.call(object, key));

// shallow-copy objectsd
const original = { a: 1, b: 2 };
const objectCopy = { ...original, c: 3 }; // copy => { a: 1, b: 2, c: 3 }

// extend array
const extendedArray = [...items, 1,2,3];

// iterable object to array
const elements = document.querySelector('p')
const nodes = [...elements];

// array like object to array
const arr = Array.from(arrLike);

// map over iterables
// bad
const baz = [...foo].map(bar);

// good
const baz = Array.from(foo, bar);

// this
// good - this is used
class Foo {
  bar() {
    console.log(this.bar);
  }
}

// good - constructor is exempt
class Foo {
  constructor() {
    // ...
  }
}

// good - static methods aren't expected to use this
class Foo {
  static bar() {
    console.log('bar');
  }
}

// good - the test is still accessible outside `if` scope
let test;
if (currentUser) {
  test = () => {
    console.log('Yup.');
  };
}

// bad - dont overide `arguments` object
function foo(name, arguments) {
  // ...
}

// bad - use spread operator instead
function concatenateAll() {
  const args = Array.prototype.slice.call(arguments);
  return args.join('');
}

// best (use the functional force)
const sum = numbers.reduce((total, num) => total + num, 0);
sum === 15;

// best (keeping it functional)
const increasedByOne = numbers.map((num) => num + 1);

// TODO: solution to problem
// FIXME: problem

// best - value -> negate boolean value -> true boolean value
// substitute for Boolean(value) type cast
const hasAge = !!age;


