## linked list using js

```js
// linked list structure
const list = {
    head: {
        value: 6
        next: {
            value: 10
            next: {
                value: 12
                next: {
                    value: 3
                    next: null
                    }
                }
            }
        }
    }
};

// create a node class
class ListNode {
    constructor(data) {
        this.data = data
        this.next = null
    }
}

// create nodes
let node1 = new ListNode(2)
let node2 = new ListNode(5)

// link node
node1.next = node2

// set linked list head
class LinkedList {
  constructor(head = null) {
      this.head = head
  }

  // return the length of linked list
  size() {
    let count = 0;
    let node = this.head;

    while (node) {
        count++;
        node = node.next
    }

    return count;
  }

  // clear linked list
  clear() {
    this.head = null;
  }

  // get last node from linked list
  getLast() {
    let lastNode = this.head;

    if (lastNode) {
        while (lastNode.next) {
            lastNode = lastNode.next
        }
    }

      return lastNode
  }

  // get first node from linked list
  getFirst() {
    return this.head;
  }
}

// linked list
let list = new LinkedList(node1)

console.log(list.head.next.data) //returns 5

// -----------------------------
removeFromHead() {
    if (this.length === 0) {
        return undefined;
    }
        
    const value = this.head.value;
    this.head = this.head.next;
    this.length--;
        
    return value;
}

find(val) {
    let thisNode = this.head;
 
    while(thisNode) {
        if(thisNode.value === val) {
            return thisNode;
        }
            
        thisNode = thisNode.next;
    }
     
    return thisNode;
}

remove(val) {
    if(this.length === 0) {
        return undefined;
    }
    
    if (this.head.value === val) {
        this.removeFromHead();
        return this;
    }
    
    let previousNode = this.head;
    let thisNode = previousNode.next;
    
    while(thisNode) {
        if(thisNode.value === val) {
            break;
        }
        
        previousNode = thisNode;
        thisNode = thisNode.next;
    }
    
    if (thisNode === null) {
        return undefined;
    }
    
    previousNode.next = thisNode.next;
    this.length--;
    return this;
}

```