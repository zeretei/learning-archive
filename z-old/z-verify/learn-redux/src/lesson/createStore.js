// redux store from scratch
const createStore = (reducer) => {
  let state;
  let listeners = [];

  // return state
  const getState = () => state;

  // dispatch an action
  const dispatch = (action) => {
    // use reducer to dispact the action
    state = reducer(state, action);

    // listen for listeners
    listeners.forEach((listener) => listener());
  };

  // add a listener
  const subscribe = (listener) => {
    // add listener to listeners array
    listeners.push(listener);

    // # subscribe
    // subscription = store.subscribe(listener);
    //
    // # unsubscribe
    // subscription();
    return () => {
      // remove listener from the listeners array
      listeners = listeners.filter((l) => l !== listener);
    };
  };

  // // remove a listener
  // const unsubscribe = (listener) => {
  //   listeners = listeners.filter((l) => l !== listener);
  // };

  // set initial state (let state) by default
  dispatch({});

  return { getState, dispatch, subscribe };
};

export { createStore };
