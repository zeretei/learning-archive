# Time vs Size
## worst case complexity
- function defined by the maximum number of steps taken on any instance of size N
## best case complexity
-  function defined by the minimum number of steps taken on any instance of size N
## best case complexity
-  function defined by the average number of steps taken on any instance of size N

# Big-O Notation

## O(1)
> constant time complexity
```py
```
## O(N)
> linear time complexity
```py
animals = ["ocelot", "octopus", "opossum", "orangutan", "orca", "oriole", "oryx", "osprey"];
target = 'orca'

for i, animal in enumerate(animals):
    if animal == target:
        print(f'Found {animal} in index {i}')
```
## O(N^n)
> N ^ n time (eg. O(N^2), O(N^3)) | O(N) O(N) === O(N^2)
```py
# 0(N^2)
def bubbleSort(arr):
    n = len(arr)
  
    for i in range(n-1):
        for j in range(0, n-i-1):
            if arr[j] > arr[j+1] :
                # swap
                arr[j], arr[j+1] = arr[j+1], arr[j]
    return arr

arr = [1,9,2,8,6,7,5,3,4,0]
  
bubbleSort(arr) # [0,1,2,3,4,5,6,7,8,9]
```

## O(log N)
> logarithmic time complexity | time goes up linearly while the N goes up exponentially
```py
def binarySearch(numbers, target):
	  low = 0
  	high = len(numbers) - 1

    while low <= high:
		    middle = low + (high - low) // 2

		    if numbers[middle] == target:
			      return middle
            
		    elif numbers[middle] < target:
			      low = middle + 1

		    else:
			      high = middle - 1

    return False


numbers = [0,1,2,3,4,5,6,7,8,9]
target = 5

index = binarySearch(numbers, target)

if not index:
	print('This item was not found in the list.')
else:
	print(f'{target} found in index {index}  of list') # 5 found in index 5 of list

```
## O(N * Log N)
> log linear time complexity
```py
```
## O(2^N)
> exponential time complexity
```py

```
## O(N!)
> factorial time complexity
```py
```



