
## System Administrator
> A person who is responsible for the upkeep, configuration, and reliable operation of computer systems; especially multi-user computers, such as servers.

```bash
find [path] [expression determines what to find] [-options] [item]

# find and delete an item
find ./path [expression] [item] -exec rm -i {} \;

# find empty file and dirs
find ./path -empty

# find  files with permission 664
find ./path -perm 664
```


```
$$: process id of the current shell
$!: proceess id of the background process
$?: exit status
!!: execute most recent command
```

jobs: show jobs running in background
fg: bring to foreground

ps: process currently running
top: top most cpu intensive processes
df: mounted filesystem & report status
du: disk usage
find: finding files on the system
kill: send signal
lsof: list open files
man: man page
**man page section**
```
 Section 1: General commands
 Section 2: System calls (functions provided by the kernel)
 Section 3: Library functions (functions within program libraries)
 Section 4: Special files (usually found in /dev)
 Section 5: File formats and conventions (eg /etc/passwd)
 Section 6: Games and screensavers
 Section 7: Miscellaneous
 Section 8: System administration commands
```
mount: mount filesystem

## info
```
stat
vmstat
strace
ulimit
```

## extracting an manipulating data
```
cat
cut
grep
awk
sed
sort
```