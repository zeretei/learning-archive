# Linux Directory Structure

**/bin: binary files**
- contains the executable files of many basic shell commands.

**/dev: device files**
- contains special files, including those relating to the devices. These are virtual files, not physically on the disk.

```
/dev/null: can be sent to destroy any file or string
/dev/zero: contains an infinite sequence of 0
/dev/random: contains an infinite sequence of random values
```
**/etc: configuration files**
- contains the core configuration files of the system.

**/usr: user binaries and program data**
- all the executable files, libraries, source of most of the system programs.

```
/usr/bin: contains basic user commands
/usr/sbin: contains additional commands for the administrator
/usr/lib: contains the system libraries
/usr/share: contains documentation or common to all libraries, for example ‘/usr/share/man’ contains the text of the manpage
```
**/home: user personal data**
- contains personal directories for the users.

**/lib: shared libraries**
- holds the libraries needed by the binaries in /bin and /sbin directories.

**/sbin: system binaries**
- contains the binaries that can only be run by root or a sudo user.

**/tmp: temporary files**
- this directory holds temporary files.

**/var: variable data files**
- where programs store runtime information like system logging, user tracking, caches, and other files that system programs create and manage.

**/boot: boot files**
- contains the files of the kernel and boot image, in addition to LILO and Grub.

**/proc: process and kernel files**
- contains the information about currently running processes and kernel parameters.

**/opt: optional software**
- used for installing/storing the files of third-party applications that are not available from the distribution’s repository.

**/root: home directory of root user**
- home directory of the root user

**/media: mount point for removable media**
- contains connected removable media such as USB disk, SD card or DVD.

**/mnt: mount directory**
- used by system administrators to manually mount a filesystem.

**/srv: service data**
- contains data for services provided by the system.






