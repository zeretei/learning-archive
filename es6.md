```js
// in a browser
let RegExp = "Hello!";
console.log(RegExp); // "Hello!"
console.log(window.RegExp === RegExp); // false
const ncz = "Hi!";
console.log(ncz); // "Hi!"
console.log("ncz" in window); // false


function is32Bit(c) {
 return c.codePointAt(0) > 0xFFFF;
}
console.log(is32Bit("𠮷")); // true
console.log(is32Bit("a")); // false

console.log(String.fromCodePoint(134071)); // "𠮷"

```

JavaScript strings assumed each 16-bit sequence, called a code unit

Normalization Form Canonical Composition ("NFC"), the default 
Normalization Form Canonical Decomposition ("NFD") 
Normalization Form Compatibility Composition ("NFKC") 
Normalization Form Compatibility Decomposition ("NFKD") 

When a regular expression has the u flag set, it switches modes to work on characters, not code units.
```js
function codePointLength(text) {
 let result = text.match(/[\s\S]/gu);
 return result ? result.length : 0;
}
console.log(codePointLength("abc")); // 3
console.log(codePointLength("𠮷bc")); // 3

// =====================================

// check if regex 'u' exists
function hasRegExpU() {
 try {
   var pattern = new RegExp(".", "u");
 return true;
 } catch (ex) {
   return false;
 }

```

The includes() method returns true if the given text is found anywhere within the string. It returns false if not. 
The startsWith() method returns true if the given text is found at the beginning of the string. It returns false if not. 
The endsWith() method returns true if the given text is found at the end of the string. It returns false if not. 
```js
console.log("x".repeat(3)); // "xxx"
```

## functions
```js
//  default parameter value TDZ
function getValue(value) {
 return value + 5;
}
function add(first, second = getValue(first)) {
 return first + second;
}
console.log(add(1, 1)); // 2
console.log(add(1)); // 7
```

rest parameters restrictions
> there can be only one rest parameter, and the rest parameter must be last
```
 When a function’s [[Construct]] method is called, new.target is filled with the target of the new operator. 
```

Block-Level Functions (continue)